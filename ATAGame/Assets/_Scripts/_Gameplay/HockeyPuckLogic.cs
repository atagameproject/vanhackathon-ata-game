﻿using UnityEngine;
using System.Collections;

public class HockeyPuckLogic : MonoBehaviour {

	public float speed = 3.0f;
	private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		rb.velocity = Vector2.left * speed;

		int y = Random.Range(2, 10);
		Vector3  temp = transform.position;
		transform.position = new Vector3(temp.x, y, temp.z);
	}
}
