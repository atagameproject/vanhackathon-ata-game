﻿using UnityEngine;
using System.Collections;
using System;

public class BananaLogic : MonoBehaviour 
{
    public event Action onKilling;

    public int shootSpeed; 
    public GameObject go;
    public Transform tr;
    private Rigidbody2D _rigidBody;
    
	void Awake () 
    {
        go = gameObject;
        tr = GetComponent<Transform>();
        _rigidBody = GetComponent<Rigidbody2D>();
	}
	
	public void Shoot (Vector2 dir, float playerSpeed)
    {
        if(_rigidBody != null)
        {
            _rigidBody = GetComponent<Rigidbody2D>();
        }
        _rigidBody.velocity = new Vector2(dir.x * (playerSpeed + shootSpeed), dir.y * (playerSpeed + shootSpeed));
        _rigidBody.angularVelocity = 360;
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.collider.CompareTag("Enemy"))
        {
            Destroy(col.gameObject);
            // dispatch killing event
            if (onKilling != null)
                onKilling();
            // turn off the banana
            OnBecameInvisible();
        }
    }

    void OnBecameInvisible()
    {
        // turning off it to be used on pooling.
        _rigidBody.velocity = Vector2.zero;
        _rigidBody.angularVelocity = 0;
        tr.localRotation = Quaternion.identity;
        go.SetActive(false);
    }
}
