﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject[] _hazards;

	[Range(0,1)]
	public float _chanceToSpawn;

	// Use this for initialization
	void Start () {
		
	}
	
	void OnDrawGizmos(){
		Gizmos.DrawWireCube(transform.position, transform.localScale);
	}

	public void Generate ()
	{
		if (Random.value<_chanceToSpawn){
			int i = Random.Range(0, _hazards.Length);
			GameObject go = Instantiate(_hazards[i], this.transform.position, Quaternion.identity) as GameObject;
			go.transform.parent = transform;
		}
	}

	public void Clean(){
		foreach (Transform child in transform){
			Destroy(child.gameObject);
		}
	}
}
