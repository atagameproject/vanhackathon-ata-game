﻿using UnityEngine;
using System.Collections;
using System;
public class PlayerLogic : MonoBehaviour 
{
    public GameObject banana;
    public int bananaPoolSize;
	public float jumpForce = 10.0f;
	[Range(0.0f, 1.0f)]
	public float lowerJumpPercentage = 0.5f;
	public float increaseFactor = 1.01f;
	public AudioClip jumpSound;
    public AudioClip scoreSound;

    private BananaLogic[] _bananaPool;
	private Vector2 pauseVelocity;

    public event Action<int> onGameOver;

    public event Action<int> onLifeChanged;
    private int _life;
    public int life {
        get { return _life; }
        set { _life = Mathf.Max(0, Mathf.Min(3, value));
        if (onLifeChanged != null)
            onLifeChanged(_life);
        }
    }

    public event Action<int> onScoreChanged;
    private int _score;
    public int score {
        get { return _score; }
        set { _score = value;
        if (onScoreChanged != null)
            onScoreChanged(_score);
        }
    }

    private bool _isJumping;
    private bool _isPaused;
    private bool _isInvencible;

	public float speed = 1;

	private Rigidbody2D rb;
    private Transform tr;
    private SpriteRenderer sp;
    private Animator anim;


    void Start () 
    {
        InputHandler input = GameObject.Find("GameplayManager").GetComponent<InputHandler>();
        input.OnTap += OnTap;
        input.OnLongTap += OnLongTap;
		input.OnSwipe += OnSwipe;

        GameplayLogic gameplay = GameObject.Find("GameplayManager").GetComponent<GameplayLogic>();
        gameplay.onPause += OnPaused;

		rb = GetComponent<Rigidbody2D>();
		tr = GetComponent<Transform>();
        sp = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();

        rb.velocity = Vector3.right * speed;

        life = 3;
        _isJumping = false;
        _isPaused = false;

        // creating bananas pooling
        _bananaPool = new BananaLogic[bananaPoolSize];
        GameObject go;
        for(int i = 0; i < bananaPoolSize; i++)
        {
            go = GameObject.Instantiate(banana);
            go.name = "Banana " + i;
            _bananaPool[i] = go.GetComponent<BananaLogic>(); 
            _bananaPool[i].go.SetActive(false);
            _bananaPool[i].onKilling += OnKilling;
        }

        StartCoroutine(Scoring());
	}


    void OnPaused(bool paused)
    {
        _isPaused = paused;
        if (paused)
        {
        	pauseVelocity = rb.velocity;
            rb.Sleep();
            anim.speed = 0;
        }
        else
        {
			rb.velocity += pauseVelocity;
            rb.WakeUp();
            anim.speed = 1;
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    { 
        // player is only able to jump again when it is on ground.
        if (_isJumping && col.gameObject.CompareTag("Floor"))
        {
            _isJumping = false;
            anim.SetTrigger("Walk");
        }
        // if collided with enemy, damage
        else if (col.gameObject.CompareTag("Enemy"))
        {
            Destroy(col.gameObject);
            StartCoroutine(Damage());
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        // if collided with obstacle, damage
        if (col.CompareTag("Obstacle") && !_isInvencible)
        {
            StartCoroutine(Damage());
        }
    }

    void OnTap() 
    {
        if (_isPaused)
            return;

        // player only can jump if it is not jumping already.
        if (!_isJumping)
        {
			rb.AddForce(new Vector2(0, jumpForce*lowerJumpPercentage), ForceMode2D.Impulse);
            _isJumping = true;
            anim.SetTrigger("Jump");
			MusicPlayer.PlayOneShot(jumpSound);
        }
    }

    void OnLongTap()
    {
        if (_isPaused)
            return;

        // player only can jump if it is not jumping already.
        if (!_isJumping)
        {
            rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
            _isJumping = true;
            anim.SetTrigger("Jump");
            MusicPlayer.PlayOneShot(jumpSound);
        }
    }

    void OnSwipe(Vector2 dir)
    {
        if (_isPaused)
            return;

        ShootBanana(dir);
    }

    void OnKilling()
    {
		MusicPlayer.PlayOneShot(scoreSound);
        score += 10;
		speed *= 1.01f;
		float y = rb.velocity.y;
		rb.velocity = Vector2.right * speed  + Vector2.up * y;
    }

    void ShootBanana(Vector2 dir)
    {
        for (int i = 0; i < bananaPoolSize; i++)
            if (!_bananaPool[i].go.activeInHierarchy)
            {
                BananaLogic banana = _bananaPool[i];
                banana.go.SetActive(true);
                banana.tr.position = tr.position;
                banana.tr.Translate(2 * dir.x, 2 * dir.y, 0);
                banana.Shoot(dir, speed);
                return;
            }
        Debug.Log("Warning: Banana ran out!");
    }

    void GameOver()
    {
        rb.velocity = Vector2.zero;
    }

    IEnumerator Damage()
    {
        _isInvencible = true;
        life--;

        if (life < 1 && onGameOver != null)
        {
            GameOver();
            onGameOver(score);
        }
        
        float time = 0;
        bool red = true;
        while (time < 2.0f)
        {
            sp.color = red ? Color.red : Color.white;
            red = !red;
            yield return new WaitForSeconds(0.25f);
            time += 0.25f;
        }
        sp.color = Color.white;
        
        _isInvencible = false;
    }

    IEnumerator Scoring()
    {
        while(life > 0)
        {
            if (!_isPaused)
            {
                score++;
				speed *= increaseFactor;

				float y = rb.velocity.y;
				rb.velocity = Vector2.right * speed  + Vector2.up * y;
            } 
            yield return new WaitForSeconds(1.0f);
        }
    }
}
