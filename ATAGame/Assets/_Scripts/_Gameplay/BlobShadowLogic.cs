﻿using UnityEngine;
using System.Collections;

public class BlobShadowLogic : MonoBehaviour {

    Transform apeTr;
    Transform tr;

    float scale;

	// Use this for initialization
	void Start () {
        apeTr = GameObject.Find("Player").GetComponent<Transform>();
        tr = GetComponent<Transform>();
        scale = 0.8f;
    }
	
	// Update is called once per frame
	void Update () {
        float offset = tr.position.y - apeTr.position.y;
        tr.position = apeTr.position;
        tr.Translate(0, offset, 4);

        float newScale = Mathf.Abs(scale / offset);
        tr.localScale = new Vector3(2 * newScale, 2 * newScale, 1);
	}
}
