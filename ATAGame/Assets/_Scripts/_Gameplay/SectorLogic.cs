﻿using UnityEngine;
using System.Collections;

public class SectorLogic : MonoBehaviour {

	GameplayLogic gameplayLogic;

	// Use this for initialization
	private Spawner[] spawners;
	void Start () {
		gameplayLogic = GameObject.FindObjectOfType<GameplayLogic>();
		spawners = GetComponentsInChildren<Spawner>();
		//Build();
	}

	private void Build ()
	{
		int i = 0;
		foreach (Spawner spawner in spawners) {
			if (i > 0) {
				spawner.Generate ();
			}
			i++;
		}
	}

	public void Rebuild(){
		foreach (Spawner spawner in spawners){
			spawner.Clean();
			spawner.Generate();
		}
	}

	void OnTriggerExit2D (Collider2D collider)
	{
        if(collider.CompareTag("Player"))
		    gameplayLogic.ChangeSector();
	}
}
