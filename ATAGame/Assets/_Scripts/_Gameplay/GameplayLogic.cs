﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameplayLogic : MonoBehaviour {

    public event Action<bool> onPause;

	public GameObject[] _sectors;

	public GameObject[] Sectors {
		get {
			return _sectors;
		}
		set {
			_sectors = value;
		}
	}

	public float sizeOfSector = 17.9f;

	private GameObject sectorsHolder;

	private Queue<GameObject> queue = new Queue<GameObject>();

	// Use this for initialization
	void Start () {
		InitSectorsObject();
	}

	private void InitSectorsObject(){
		sectorsHolder = GameObject.Find("Sectors");
		if (!sectorsHolder){
			sectorsHolder = new GameObject("Sectors");
		}
		Vector3 diff = Vector3.right*sizeOfSector;
		for (int i=0;i<_sectors.Length;i++){
			GameObject prefab = _sectors[i];
			GameObject go = Instantiate(prefab, transform.position+diff*i, Quaternion.identity) as GameObject;
			go.transform.parent = sectorsHolder.transform;
			queue.Enqueue(go);
		}
	}

	public void ChangeSector ()
	{
		GameObject go = queue.Dequeue ();
		SectorLogic sector = go.GetComponent<SectorLogic> ();
		if (sector) {
			queue.Enqueue(sector.gameObject);
			sector.Rebuild();
			sector.transform.position = sector.transform.position+Vector3.right*queue.Count*sizeOfSector;
		} else {
			Debug.LogError("Not found Sector initiated");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Pause()
    {
        if (onPause != null)
            onPause(true);
    }

    public void Resume()
    {
        if (onPause != null)
            onPause(false);
    }
}
