﻿using UnityEngine;
using System.Collections;

public class CameraFollower : MonoBehaviour
{
    Transform _transform;
    Transform _followed;

    private Vector3 offset;

	void Start () 
    {
        _transform = GetComponent<Transform>();
        _followed = GameObject.Find("Player").GetComponent<Transform>();
		offset = _transform.position-_followed.position;
	}
	
	void Update () 
    {
    	//podemos fazer assim?
		//_transform.position = _followed.position+offset;

        if(_followed.position.x+offset.x > _transform.position.x)
        {
            Vector3 _position = _transform.position;
            _position.x = _followed.position.x+offset.x;
            _transform.position = _position;
        }
	}
}
