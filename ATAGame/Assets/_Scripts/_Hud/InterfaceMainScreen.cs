﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class InterfaceMainScreen : MonoBehaviour {

	private MusicPlayer musicPlayer;
	private Button musicButton;

	void Start(){
		musicPlayer = MusicPlayer.GetMusicPlayer();
		musicButton = this.transform.Find("MusicButton").GetComponent<Button>();
		musicButton.GetComponent<ImageSwapper>().Swap(musicButton.GetComponent<Image>(), musicButton, PlayerPrefsManager.IsMuted());
	}

	void Update(){
		if (Input.GetKey(KeyCode.Escape)){
			Application.Quit();
		}
	}

	public void OnPlayGameClicked()
    {
        SceneManager.LoadScene("Gameplay");
    }

    public void OnSoundButtonClicked()
    {
        
    }

    public void OnMusicButtonClicked()
    {
		if (musicPlayer)
			musicPlayer.ChangeMuteState();
		musicButton.GetComponent<ImageSwapper>().Swap(musicButton.GetComponent<Image>(), musicButton, PlayerPrefsManager.IsMuted());
    }

}
