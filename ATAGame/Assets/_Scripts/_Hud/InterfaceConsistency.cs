﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InterfaceConsistency : MonoBehaviour {

    GameplayLogic gameplay;

    Text scoreValue;
    Image[] lifes;

    Button pauseButton;
    GameObject pauseScreen;
    GameObject gameOverScreen;
    GameObject tutorialScreen;
    
	// Use this for initialization
	void Start () 
    {
        gameplay = GameObject.Find("GameplayManager").GetComponent<GameplayLogic>();

        scoreValue = GameObject.Find("ScoreValue").GetComponent<Text>();
        lifes = new Image[3];
        lifes[0] = GameObject.Find("Life1").GetComponent<Image>();
        lifes[1] = GameObject.Find("Life2").GetComponent<Image>();
        lifes[2] = GameObject.Find("Life3").GetComponent<Image>();
		pauseButton = GameObject.Find("PauseButton").GetComponent<Button>();
        pauseButton.interactable = false;
        pauseScreen = GameObject.Find("PauseScreen");
        pauseScreen.SetActive(false);
        gameOverScreen = GameObject.Find("GameOverScreen");
        gameOverScreen.SetActive(false);
        tutorialScreen = GameObject.Find("TutorialScreen");
        
        PlayerLogic player = GameObject.Find("Player").GetComponent<PlayerLogic>();
        player.onLifeChanged += OnLifeChanged;
        player.onScoreChanged += OnScoreChanged;
        player.onGameOver += OnGameOver;

        // pause the game to show tutorial Screen and 
        // register to OnTap event to start the game as soon as the player touch the screen
		gameplay.Pause();
		InputHandler inputHandler = gameplay.GetComponent<InputHandler>();
		inputHandler.OnTap += CloseTutorialScreen;
		inputHandler.OnLongTap += CloseTutorialScreen;
    }

    void CloseTutorialScreen()
    { 
        // if tutorial screen is actived, hides it and start the gameplay.
        if(tutorialScreen.activeInHierarchy)
        {
            tutorialScreen.SetActive(false);
			gameplay.Resume();
			gameplay.GetComponent<InputHandler>().OnTap -= CloseTutorialScreen;
			gameplay.GetComponent<InputHandler>().OnLongTap -= CloseTutorialScreen;
        	pauseButton.GetComponent<Button>().interactable = true;
        }
    }

    public void OnPauseButtonClicked()
    {
        gameplay.Pause();
        pauseScreen.SetActive(true);
        pauseButton.gameObject.SetActive(false);
    }

    public void OnResumeButtonClicked()
    {
        gameplay.Resume();
        pauseScreen.SetActive(false);
		pauseButton.gameObject.SetActive(true);
    }

    void OnScoreChanged(int score)
    {
        scoreValue.text = score.ToString();
    }

    void OnLifeChanged(int life)
    {
        lifes[0].enabled = life > 0;
        lifes[1].enabled = life > 1;
        lifes[2].enabled = life > 2;
    }

    void OnGameOver(int score)
    {
    	int[] highscore = PlayerPrefsManager.SaveHighscore(score);
        Text gameOverScore = gameOverScreen.transform.FindChild("PlayerScore").GetComponent<Text>();
        gameOverScore.text = "Your Score: " + score.ToString();
        gameOverScreen.SetActive(true);
		Text highscoreText = gameOverScreen.transform.FindChild("HighScores").GetComponent<Text>();
		UpdateHighscoreText(highscoreText, highscore);
        pauseButton.interactable = false;
    }

	private void UpdateHighscoreText(Text text, int[] highscore){
		text.text = "1: " + highscore[0].ToString() + "\n" + 
					"2: " + highscore[1].ToString() + "\n" +
					"3: " + highscore[2].ToString();
    }

    public void OnRetryClicked()
    {
        SceneManager.LoadScene("Gameplay");    
    }

    public void OnMainScreenClicked()
    {
        SceneManager.LoadScene("Mainscreen");
    }
}
