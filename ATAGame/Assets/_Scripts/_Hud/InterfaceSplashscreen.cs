﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class InterfaceSplashscreen : MonoBehaviour {

	public float _timeToLoad = 3.0f;

	// Use this for initialization
	void Start () {
		Invoke("LoadNext", _timeToLoad);
	}

	void LoadNext(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
	}
}
