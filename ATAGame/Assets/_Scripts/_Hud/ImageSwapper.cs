﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageSwapper : MonoBehaviour {

	public Sprite offImage;
	public Sprite offImagePressed;
	public Sprite onImage;
	public Sprite onImagePressed;

	public void Swap (Image image, Button button, bool isOn)
	{
		SpriteState ss = button.spriteState;
		if (isOn) {
			image.sprite=onImage;
			ss.pressedSprite = onImagePressed;
		} else {
			image.sprite=offImage;
			ss.pressedSprite = offImagePressed;
		}
		button.spriteState = ss;
	}
}
