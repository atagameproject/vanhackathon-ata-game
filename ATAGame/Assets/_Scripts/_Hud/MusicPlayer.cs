﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

	public AudioClip[] backgroundMusic;

    private AudioSource _audioSource;
    private bool isMuted;

    void Awake() {
        DontDestroyOnLoad(transform.gameObject);
        isMuted = PlayerPrefsManager.IsMuted();
    }

	// Use this for initialization
	void Start ()
	{
		_audioSource = GetComponent<AudioSource> ();
		_audioSource.clip = backgroundMusic [0];

		if (isMuted) {
			MuteBackground ();
		} else {
			UnMuteBackground();
		}

		_audioSource.Play();
	}

	public void ChangeMuteState ()
	{
		if (isMuted) {
			UnMuteBackground();
		} else {
			MuteBackground();
		}

		isMuted = !isMuted;
		PlayerPrefsManager.Mute(isMuted);
	}

	public void UnMuteBackground ()
	{
		_audioSource.volume = 1.0f;
	}

	public void MuteBackground ()
	{
		_audioSource.volume = 0.0f;
	}

	public static MusicPlayer GetMusicPlayer ()
	{
		MusicPlayer musicplayer = GameObject.FindObjectOfType<MusicPlayer>();
		if (musicplayer)
			return musicplayer;
		Debug.LogWarning("There is no music player in this scene.");
		return null;
	}

	void OnLevelWasLoaded(int level) {
		_audioSource.Stop();
        _audioSource.clip = backgroundMusic[level];
		_audioSource.Play();
	}

    public static void PlayOneShot (AudioClip audio)
	{
		MusicPlayer mp = GetMusicPlayer();
		if (mp)
			mp._audioSource.PlayOneShot(audio);
	}
}
