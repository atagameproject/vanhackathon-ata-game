﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class InputHandler : MonoBehaviour 
{

    [Range (0.0f, 0.2f)]
    public float timeLongJump = 0.1f;

    [Tooltip("This is the percentage of the widscreen size that will be used to differenciate what is a swipe or a tap.")]
	[Range (0.0f, 0.5f)]
    public float inchesToSwipe = 0.1f;

    public event Action OnTap;
    public event Action OnLongTap;
    public event Action<Vector2> OnSwipe;

    private Vector2 _lastTouch;
	private float _startTouchTime;
    private int tapMaxOffset = 10;

    private bool _isPaused;
    private GameObject _tutorialScreen;

    void Start()
    {
        _lastTouch.x = -1;
		tapMaxOffset = Mathf.RoundToInt(Screen.dpi * inchesToSwipe);

        _tutorialScreen = GameObject.Find("TutorialScreen");

        // registering to the pause event
        GameObject.Find("GameplayManager").GetComponent<GameplayLogic>().onPause += delegate(bool p) { _isPaused = p; };
    }

    void Update ()
	{
		// only check for input if it is not paused and tutorial has been hidden.
		if (_isPaused && !_tutorialScreen.activeInHierarchy) {
        	_lastTouch.x = -1;
			return;
		}

		if (Input.GetMouseButtonDown (0)) {
			// on start touch event
			if (_lastTouch.x == -1) {
				// time 
				_startTouchTime = Time.realtimeSinceStartup;
				_lastTouch = Input.mousePosition; 
			}
		} else if (Input.GetMouseButtonUp (0)) {
			// on stop touch event
			if (_lastTouch.x != -1) {
				Vector2 touch = Input.mousePosition;
				if (Vector2.Distance (touch, _lastTouch) < tapMaxOffset) {
					float deltaTime = Time.realtimeSinceStartup - _startTouchTime;
					if (deltaTime < timeLongJump) {
						if (OnTap != null) {
							OnTap ();
						}
					} else if (OnLongTap != null) {
						OnLongTap ();
					}
                }
                else if (OnSwipe != null)
                {
                    Vector2 dir = touch - _lastTouch;
                    OnSwipe(dir.normalized);
                }
                _lastTouch.x = -1;
            }
		} else if (Input.GetKey(KeyCode.Escape)){
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
		}
	}
}
