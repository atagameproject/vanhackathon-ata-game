﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class PlayerPrefsManager{

	public static string FIRST_KEY = "FIRST";
	public static string SECOND_KEY = "SECOND";
	public static string THIRD_KEY = "THIRD";
	public static string MUTE_KEY = "MUTE";

	public static int[] LoadHighscore ()
	{
		int[] highscore = new int[3]{0, 0, 0};
		if (PlayerPrefs.HasKey(FIRST_KEY))
			highscore[0] = PlayerPrefs.GetInt(FIRST_KEY);
		if (PlayerPrefs.HasKey(SECOND_KEY))
			highscore[1] = PlayerPrefs.GetInt(SECOND_KEY);
		if (PlayerPrefs.HasKey(THIRD_KEY))
			highscore[2] = PlayerPrefs.GetInt(THIRD_KEY);

		return highscore;
	}

	public static int[] SaveHighscore (int score)
	{
		List<int> values = new List<int> (LoadHighscore ());
		int i = 0;
		while (i < values.Count () && score < values [i]) {
			i++;
		}
		if (i < values.Count ()) {
			values.Insert (i, score);

			string[] keys = new string[]{ FIRST_KEY, SECOND_KEY, THIRD_KEY };
			for (i = 0; i < keys.Length; i++) {
				PlayerPrefs.SetInt (keys [i], values [i]);
			}
			PlayerPrefs.Save ();
		}
		return values.GetRange(0, 3).ToArray();
	}

	public static bool IsMuted ()
	{
		if (!PlayerPrefs.HasKey (MUTE_KEY)) {
			return false;
		}
		return PlayerPrefs.GetInt(MUTE_KEY)==1;
	}

	public static void Mute (bool mute)
	{
		PlayerPrefs.SetInt(MUTE_KEY, (mute)?1:0);
	}
}
